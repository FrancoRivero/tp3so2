/*
 ============================================================================
 Name        : Baash.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "parser.h"
#include "command.h"

void mainloop();
void set_pipes(char*);

int main(int argc, char* argv[]) {

	setenv ("HOSTNAME", "USER", 1);//Seteamos el valor de Hostname a user

	mainloop();

	return EXIT_SUCCESS;
}

/*
 * Muestra el hostname y el directorio actual
 * Queda espectante al input como el baash
 * Y ejeucta funciones para primero interpretar el comando y despues ejecutarlo
 */
void mainloop(){
	char input[256];
		char currentdir[1024];

		do
		{
			if (getcwd(currentdir, sizeof(currentdir)) == NULL){//El directorio de trabajo
				       printf("getcwd() error");
			}

			printf("%s@%s:~%s$ ", getenv("USER"), getenv("HOSTNAME"), currentdir);//Muestra nombre y posicion actual
			gets(input);
			set_pipes(input);
		}while (!feof(stdin) && strstr(input, "exit") == NULL); //Termina el programa en el comando exit o al hacer Ctrl+D
}

/*
 * Separa los comandos segun pipes, crea los pipes y ejecuta los comandos
 */
void set_pipes(char *input_string){
	struct parser_element commands[10];// Obtiene la secuencia de comandos en pipes
	int num_commands=0;

	parse_pipes(input_string, &num_commands, &commands[0]);

	int pipes[num_commands][2];
	//creo todos los pipes
	int i;
	for(i=0; i < num_commands-1; i++){
		if(pipe(pipes[i])<0) {
			perror("Error creating pipe!");
			exit(1);
		}
	}

	int counter=0;
	int pid;
	while(counter < num_commands){
		//Checkeo que no sea builtin
		if(!built_in_command(&commands[counter])){
			pid = fork();
			if (pid==0){
				if(!counter==0){//Si no es el primer comando
					if(dup2(pipes[counter-1][0],0)< 0){
						perror("Error with dup!");
					}
				}
				if(!counter==(num_commands-1)){ //Si no es el ultimo comando
					if(dup2(pipes[counter][1],1) < 0){
							perror("Error with dup!");
					}
				}
				command_search_logic(&commands[counter]);
				exit(0);
			}
			if(!commands[counter].background_process)
				wait(0);
			if(counter >0)
				close(pipes[counter-1][0]);
			if(counter<(num_commands-1))
				close(pipes[counter][1]);
		}
		counter++;
	}
}


