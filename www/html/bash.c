/*
				* Muestra el hostname y el directorio actual
				* Queda espectante al input como el baash
				* Y ejeucta funciones para primero interpretar el comando y despues ejecutarlo
				*/
				/**/
				/*Se recibe el comando desde el cliente y se decide que hacer con ese comando*/
				do
				{
					memset( buf, 0, TAM );
					n = read( newsockfd, buf, TAM-1 );
					if ( n < 0 ) 
					{
						perror( "reading to socket" );
						exit(1);
					}
					else{
						buf[strlen(buf)] = '\0';
						if(buf[0] == 'c'){
							if (getcwd(currentdir, sizeof(currentdir)) == NULL){//El directorio de trabajo
								       printf("getcwd() error");
							}
							n = write( newsockfd, currentdir, TAM-1 );
							if ( n < 0 ) 
							{
								perror( "writing to socket" );
								exit( 1 );
							}
						}

					}
					memset( buf, 0, TAM );
					printf("%s\n",buf );
					n = read( newsockfd, buf, TAM-1 );
					if ( n < 0 ) 
					{
						perror( "reading to socket" );
						exit(1);
					}

					
					buf[strlen(buf)] = '\0';

					if(!strcmp(buf,"exit"))
					{
						disconnect();
						exit(0);
					}
					else if(!strcmp( "download",strtok(buf,"/")))
					{
						strcpy(data.file,strtok(NULL,"/"));
					    strcpy(data.port_udp,strtok(NULL,"\0"));
					    strcpy(data.message, "Recibiendo archivo.\n");
					    data.flag = 1;
					    n = write( newsockfd, data.message, TAM-1 );
						if ( n < 0 ) 
						{
							perror( "writing to socket" );
							exit( 1 );
						}
						break;
					}
					else
					{
						dup2( newsockfd, STDOUT_FILENO );  /* duplicate socket on stdout */
						set_pipes(buf);
						fflush(stdout);
					}
					
					
				}while (!feof(stdin) && strstr(buf, "exit") == NULL); //Termina el programa en el comando exit o al hacer Ctrl+D
